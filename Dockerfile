FROM alpine:3.6

ENV GPG_TTY /dev/console
ENV GNUPGHOME /root/lockbox


COPY lib/* /usr/local/sbin/
RUN chmod +x /usr/local/sbin/doit.sh

RUN apk --no-cache add bash \
  gnupg \
  util-linux \
  cryptsetup \
  e2fsprogs \
  expect \
  rng-tools

ENTRYPOINT ["/usr/local/sbin/doit.sh"]
CMD []
