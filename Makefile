run: build
	clear
	docker run -v ${PWD}/secure:/root/localdump -it --privileged samlachance/lockbox

subkeys: build
	clear
	docker run -v ${PWD}/secure:/root/localdump -it --privileged --env SUBKEYS=2 samlachance/lockbox

automated: build
	clear
	docker run -v ${PWD}/secure:/root/localdump -it --privileged \
		--env SUBKEYS=2 \
		--env LBPASS=asdf \
		--env NAME="Tyler Durden" \
		--env EMAIL="tyler@paperstreetsoap.co" \
		--env PASS="asdfjkl;asdfjkl;" \
		samlachance/lockbox

build:
	docker build -t samlachance/lockbox:latest .
