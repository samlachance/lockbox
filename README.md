## About

Lockbox is a docker image that is used to securely generate PGP keys. Running lockbox will generate a LUKS encrypted iso, build a filesystem, generate PGP keys in the newly created lockbox volume, and then dump the encrypted iso to your local filesystem. At no point do your keys touch an unencrypted volume. It is suggested that you generate these keys on a computer that does not have access to the internet.

## How to launch

Note: The following command must be run as root.

```
docker run -v /local/dump/directory/here:/root/localdump -it --privileged samlachance/lockbox
```

## How to generate keys

Lockbox will guide you through the process of generating your keys. Follow the prompts closely. Note that the passphrases for your encrypted volume and for your key should be strong and unique.

## What next?

After you successfully generate your key, lockbox will dump the encrypted volume containing your private key and your public key into whatever directory you specified in your `docker run` command.

From here, you can decrypt and mount the iso to any computer:

Decrypt

```
 cryptsetup luksOpen lockbox.iso lockbox_volume
```

Mount

```
mount /dev/mapper/lockbox_volume /mount/directory/here
```

Unmount

```
umount /mount/directory/here
```

Encrypt

```
cryptsetup luksClose lockbox_volume
```

## Credits

A HUGE thanks to [Bobby](https://github.com/brb3) for reviving the project and rewriting nearly all of the code. He took my basic POC and turned it into something that someone would actually use. Thanks!