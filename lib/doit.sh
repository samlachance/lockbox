#!/bin/sh
set -e

trap ctrl_c INT

BOLD="\033[1m"
ITALIC="\033[3m"
CLEAR="\033[0m"
RED="\033[31m"
BLUE="\033[34m"
GREEN="\033[32m"
ARROW="$BOLD$BLUE --> $CLEAR"

# Set a default value for SUBKEYS
if [[ -z "${SUBKEYS}" ]]; then
    SUBKEYS=0
fi

do_it() {
    # Use rng-tools for moar entropy
    rngd -r /dev/urandom

    echo -e "${BOLD}${ITALIC}
\tWelcome to Lockbox ${CLEAR}\n
${ARROW}Starting up..." &&
    cd root/ &&
    echo -e "${ARROW}Building lockbox..."
    fallocate -l 10M lockbox.iso &&

    echo -e "${ARROW}Encrypting lockbox. Please follow the prompts."
    print_step 1 "This step creates a secure volume (lockbox.iso) to store your private key."
    if [[ -z "${LBPASS}" ]]; then
        prompt_password
    fi
    echo -e "\n${ARROW}Building secure volume..."
    env LBPASS=$LBPASS expect /usr/local/sbin/step1.exp 1>/dev/null

    echo -e "${ARROW}Making the lockbox available..."
    env LBPASS=$LBPASS expect /usr/local/sbin/open_volume.exp 1>/dev/null

    echo -e "${ARROW}Building lockbox filesystem..."
    mkfs.ext3 /dev/mapper/lockbox_volume 1>/dev/null

    echo -e "${ARROW}Mounting lockbox..."
    mkdir lockbox
    mount /dev/mapper/lockbox_volume lockbox

    echo -e "${ARROW}Lockbox is ready."
    sleep 1

    echo -e "${ARROW}Creating keys. Please follow the prompts."
    print_step 2 "This step creates your actual PGP keys.\n
\tMake sure you use a ${BOLD}secure${CLEAR} password."
    if [[ -z "${NAME}" ]]; then
        echo -n "Enter your real name: "
        read NAME
    fi
    if [[ -z "${EMAIL}" ]]; then
        echo -n "Enter your email address: "
        read EMAIL
    fi
    echo -e "Generating key. This can take a while..."
    if [[ -z "${PASS}" ]]; then
        env NAME="${NAME}" EMAIL=$EMAIL expect /usr/local/sbin/step2.exp 1>/dev/null
    else
        echo "Key-Type: RSA
             Passphrase: ${PASS}
             Name-Real: ${NAME}
             Name-Email: ${EMAIL}
             Key-Length: 4096" | gpg --generate-key --batch 1> /dev/null
    fi

    echo -e "\n${ARROW}${GREEN}${BOLD}${ITALIC}Successfully generated keys!${CLEAR}\n"

    # If the user wants subkeys, make 'em!
    if [ $SUBKEYS > 0 ]; then
        build_subkeys ${SUBKEYS}
    fi

    echo -e "${ARROW}Exporting public key..."
    gpg --armor --export > lockbox.pub.asc 2>/dev/null
    gpgconf --kill gpg-agent
    echo -e "${ARROW}Unmounting lockbox..."

    # Sometimes gpg-agent doesn't have enough time to shut down.
    sleep 3
    umount lockbox

    echo -e "${ARROW}Finalizing encryption..."
    cryptsetup luksClose lockbox_volume
    echo -e "${ARROW}Moving secure lockbox to your local filesystem."
    cp lockbox.iso localdump/
    echo -e "${ARROW}Moving public key to your local filesystem"
    cp lockbox.pub.asc localdump/
    echo -e "\t${BOLD}${ITALIC}Done.${CLEAR}"
    exit 0
}

print_step() {
    echo -en "======\n${BOLD}Step $1: ${CLEAR}"
    echo -e $2
    echo "======"
}

ctrl_c() {
    # If the user tries to kill the command, make sure to close any created luks volume
    echo -e "\n\n${RED}Captured SIGINT. Exiting cleanly...${CLEAR}"
    gpgconf --kill gpg-agent
    `umount -l /root/lockbox 2>/dev/null ; cryptsetup luksClose lockbox_volume 2> /dev/null`
    exit
}

build_subkeys() {
    if [ $1 > 1 ]; then
        echo -e "${ARROW}Building $1 subkeys. This can take a while..."
    else
        echo -e "${ARROW}Building a subkey. This can take a while..."
    fi

    for i in `seq 1 $1`; do
        if [[ -z "${PASS}" ]]; then
            env EMAIL=$EMAIL PASS=$PASS expect /usr/local/sbin/subkey.exp 1>/dev/null
        else
            echo "Subkey generation not supported in automated mode."
        fi
    done
}

prompt_password() {
    VERIFIED=0
    while [ $VERIFIED == 0 ]; do
        echo -n "Enter a password for your secure volume: "
        read -s LBPASS
        echo
        echo -n "Verify password: "
        read -s PASS2
        if [ $LBPASS == $PASS2 ]; then
            VERIFIED=1
        else
            echo -e "${RED}Passwords did not match. Try again.\n${CLEAR}"
        fi
    done
}

do_it
